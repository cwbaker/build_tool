//
// types.cpp
// Copyright (c) Charles Baker. All rights reserved.
//

#include "types.hpp"

const char* sweet::build_tool::BUILD_TOOL_TYPE = "build.BuildTool";
const char* sweet::build_tool::TARGET_TYPE = "build.Target";
const char* sweet::build_tool::TARGET_PROTOTYPE_TYPE = "build.TargetPrototype";
