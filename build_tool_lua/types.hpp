#ifndef SWEET_BUILD_TOOL_TYPES_HPP_INCLUDED
#define SWEET_BUILD_TOOL_TYPES_HPP_INCLUDED

namespace sweet
{
    
namespace build_tool
{
    
extern const char* BUILD_TOOL_TYPE;
extern const char* TARGET_TYPE;
extern const char* TARGET_PROTOTYPE_TYPE;

}

}

#endif
